using Microsoft.AspNetCore.Builder;

var builder = WebApplication.CreateBuilder();

builder.Services.AddSingleton<ICalcService, CalcService>();
builder.Services.AddTransient<ITimeOfDay, TimeOfDayService>();

var app = builder.Build();

app.UseMiddleware<CalculatorMiddleware>();

app.MapGet("/time", async (HttpContext context) =>
{
    var timeofDay = app.Services.GetService<ITimeOfDay>();
    await context.Response.WriteAsync($"Time: {timeofDay?.GetTimeOfDay()}");
});

app.Run();


class CalculatorMiddleware
{

    private readonly RequestDelegate next;

    public CalculatorMiddleware(RequestDelegate next)
    {

        this.next = next;

    }

    public async Task InvokeAsync(HttpContext context, ICalcService calcService)
    {

        context.Response.ContentType = "text/html;charset=utf-8";
        string num1Value = context.Request.Query["num1"];
        string num2Value = context.Request.Query["num2"];

        if (string.IsNullOrWhiteSpace(num1Value) || string.IsNullOrWhiteSpace(num2Value))
        {
            context.Response.StatusCode = 400;
            await context.Response.WriteAsync("Query parameters are required.");
            return;
        }

        if (!double.TryParse(num1Value, out var num1) || !double.TryParse(num2Value, out var num2))
        {
            context.Response.StatusCode = 400;
            await context.Response.WriteAsync("Invalid 'num1' or 'num2' query parameters.");
            return;
        }

        double resultAdd = calcService.Add(num1, num2);
        double resultSubtract = calcService.Subtract(num1, num2);
        double resultMultiply = calcService.Multiply(num1, num2);
        double resultDivide = calcService.Divide(num1, num2);


        await context.Response.WriteAsync($"<h4>Result of Addition: {resultAdd}</h4>");
        await context.Response.WriteAsync($"<h4>Result of Subtraction: {resultSubtract}</h4>");
        await context.Response.WriteAsync($"<h4>Result of Multiply: {resultMultiply}</h4>");
        await context.Response.WriteAsync($"<h4>Result of Divide: {resultDivide}</h4>");
        await next.Invoke(context);
    }

}

public interface ICalcService
{
    double Add(double num1, double num2);
    double Subtract(double num1, double num2);
    double Multiply(double num1, double num2);
    double Divide(double num1, double num2);
}

public class CalcService : ICalcService
{
    public double Add(double num1, double num2)
    {
        return num1 + num2;
    }

    public double Subtract(double num1, double num2)
    {
        return num1 - num2;
    }

    public double Multiply(double num1, double num2)
    {
        return num1 * num2;
    }

    public double Divide(double num1, double num2)
    {
        if (num2 == 0)
        {
            throw new ArgumentException("Division by zero is prohibited.");
        }

        return num1 / num2;
    }
}

public interface ITimeOfDay
{
    string GetTimeOfDay();
}


public class TimeOfDayService: ITimeOfDay
{
    public string GetTimeOfDay()
    {
        var currentTime = DateTime.Now;

        switch (currentTime.Hour)
        {
            case int hour when hour >= 12 && hour < 18:
                return "It's currently daytime";

            case int hour when hour >= 18 && hour < 24:
                return "It's currently evening";

            case int hour when hour >= 0 && hour < 6:
                return "It's currently nighttime";

            default:
                return "It's currently morning";
        }
    }

}